from django.db import models
from ..forecast.model import Forecast


class UniqueAnswer(models.Model):
    date_cr = models.DateTimeField(auto_now_add=True)
    answer = models.IntegerField(blank=False, null=False)
    forecast = models.OneToOneField(Forecast, related_name='unique_answer', on_delete=models.NOT_PROVIDED)

    class Meta:
        unique_together = ['forecast']
        ordering = ['date_cr']
