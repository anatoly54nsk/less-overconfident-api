from rest_framework import serializers
from .model import UniqueAnswer
from ..forecast.model import Forecast


class UniqueAnswerSerializer(serializers.HyperlinkedModelSerializer):
    forecast = serializers\
        .HyperlinkedRelatedField(queryset=Forecast.objects.all(), many=False, view_name='forecast-detail')

    class Meta:
        model = UniqueAnswer
        fields = ['id', 'date_cr', 'answer', 'forecast', 'url']
