from rest_framework import viewsets
from .model import UniqueAnswer
from .serializer import UniqueAnswerSerializer


class UniqueAnswerViewSet(viewsets.ModelViewSet):
    queryset = UniqueAnswer.objects.all()
    serializer_class = UniqueAnswerSerializer
