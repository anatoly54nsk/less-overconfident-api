from rest_framework import serializers
from .model import Forecast
from ..task.model import Task
from ..user.model import User


class ForecastSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers\
        .HyperlinkedRelatedField(queryset=User.objects.all(), many=False, view_name='user-detail')
    task = serializers\
        .HyperlinkedRelatedField(queryset=Task.objects.all(), many=False, view_name='task-detail')
    unique_answer = serializers.HyperlinkedRelatedField(read_only=True, many=False, view_name='uniqueanswer-detail')
    cumulative_answers = serializers\
        .HyperlinkedRelatedField(read_only=True, many=True, view_name='cumulativeanswer-detail')

    class Meta:
        model = Forecast
        fields = [
            'id',
            'value',
            'probability',
            'date_begin',
            'date_end',
            'task',
            'unique_answer',
            'cumulative_answers',
            'user',
            'url',
        ]
