from rest_framework import viewsets
from .model import Forecast
from .serializer import ForecastSerializer


class ForecastViewSet(viewsets.ModelViewSet):
    queryset = Forecast.objects.all()
    serializer_class = ForecastSerializer
