from django.db import models
from ..task.model import Task
from ..user.model import User


class Forecast(models.Model):
    value = models.IntegerField(blank=False)
    probability = models.IntegerField(blank=False)
    date_begin = models.DateTimeField()
    date_end = models.DateTimeField()
    task = models.ForeignKey(Task, related_name='forecasts', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='forecasts', on_delete=models.CASCADE)

    class Meta:
        ordering = ['user']
