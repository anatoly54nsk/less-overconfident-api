from rest_framework import viewsets
from .model import TaskAnswerType
from .serializer import TaskAnswerTypeSerializer


class TaskAnswerTypeViewSet(viewsets.ModelViewSet):
    queryset = TaskAnswerType.objects.all()
    serializer_class = TaskAnswerTypeSerializer
