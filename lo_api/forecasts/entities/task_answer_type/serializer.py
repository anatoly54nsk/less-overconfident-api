from rest_framework import serializers
from .model import TaskAnswerType


class TaskAnswerTypeSerializer(serializers.HyperlinkedModelSerializer):
    tasks = serializers.HyperlinkedRelatedField(read_only=True, many=True, view_name='task-detail')

    class Meta:
        model = TaskAnswerType
        fields = ['id', 'name', 'tasks', 'url']
