from django.db import models
from ..task_category.model import TaskCategory
from ..task_answer_type.model import TaskAnswerType


class Task(models.Model):
    description = models.CharField(max_length=100, blank=False)
    task_category = models.ForeignKey(TaskCategory, related_name='tasks', on_delete=models.CASCADE)
    task_answer_type = models.ForeignKey(TaskAnswerType, related_name='tasks', on_delete=models.CASCADE)

    class Meta:
        ordering = ['description']
