from rest_framework import serializers
from .model import Task
from ..task_category.model import TaskCategory
from ..task_answer_type.model import TaskAnswerType


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    task_category = serializers\
        .HyperlinkedRelatedField(queryset=TaskCategory.objects.all(), many=False, view_name='taskcategory-detail')
    task_answer_type = serializers\
        .HyperlinkedRelatedField(queryset=TaskAnswerType.objects.all(), many=False, view_name='taskanswertype-detail')
    forecasts = serializers.HyperlinkedRelatedField(read_only=True, many=True, view_name='forecast-detail')

    class Meta:
        model = Task
        fields = ['id', 'description', 'task_category', 'task_answer_type', 'forecasts', 'url']
