from rest_framework import serializers
from .model import User


class UserSerializer(serializers.HyperlinkedModelSerializer):
    forecasts = serializers.HyperlinkedRelatedField(read_only=True, many=True, view_name='forecast-detail')

    class Meta:
        model = User
        fields = ['id', 'telegram_id', 'forecasts', 'url']
