from django.db import models


class User(models.Model):
    telegram_id = models.CharField(max_length=100, blank=False)

    class Meta:
        ordering = ['telegram_id']
