from rest_framework import serializers
from .model import TaskCategory


class TaskCategorySerializer(serializers.HyperlinkedModelSerializer):
    tasks = serializers.HyperlinkedRelatedField(read_only=True, many=True, view_name='task-detail')

    class Meta:
        model = TaskCategory
        fields = ['id', 'name', 'tasks', 'url']
