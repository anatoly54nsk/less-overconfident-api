from rest_framework import viewsets
from .model import TaskCategory
from .serializer import TaskCategorySerializer


class TaskCategoryViewSet(viewsets.ModelViewSet):
    queryset = TaskCategory.objects.all()
    serializer_class = TaskCategorySerializer
