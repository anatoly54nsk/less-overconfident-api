from django.db import models
from ..forecast.model import Forecast


class CumulativeAnswer(models.Model):
    answer = models.IntegerField(blank=False)
    date_cr = models.DateTimeField(auto_now_add=True)
    forecast = models.ForeignKey(Forecast, related_name='cumulative_answers', on_delete=models.NOT_PROVIDED)

    class Meta:
        ordering = ['date_cr']
