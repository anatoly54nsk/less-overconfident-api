from rest_framework import serializers
from .model import CumulativeAnswer
from ..forecast.model import Forecast


class CumulativeAnswerSerializer(serializers.HyperlinkedModelSerializer):
    forecast = serializers.HyperlinkedRelatedField(queryset=Forecast.objects.all(), many=False, view_name='forecast-detail')

    class Meta:
        model = CumulativeAnswer
        fields = ['id', 'answer', 'date_cr', 'forecast', 'url']
