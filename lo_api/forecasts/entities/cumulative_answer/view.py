from rest_framework import viewsets
from .model import CumulativeAnswer
from .serializer import CumulativeAnswerSerializer


class CumulativeAnswerViewSet(viewsets.ModelViewSet):
    queryset = CumulativeAnswer.objects.all()
    serializer_class = CumulativeAnswerSerializer
