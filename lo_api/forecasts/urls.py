from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views


router = DefaultRouter()

router.register(r'task_category', views.TaskCategoryViewSet, basename='taskcategory')
router.register(r'task_answer_type', views.TaskAnswerTypeViewSet, basename='taskanswertype')
router.register(r'user', views.UserViewSet, basename='user')
router.register(r'task', views.TaskViewSet, basename='task')
router.register(r'forecast', views.ForecastViewSet, basename='forecast')
router.register(r'unique_answer', views.UniqueAnswerViewSet, basename='uniqueanswer')
router.register(r'cumulative_answer', views.CumulativeAnswerViewSet, basename='cumulativeanswer')

urlpatterns = [
    path('', include(router.urls))
]
